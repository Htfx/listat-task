cmake_minimum_required(VERSION 3.12)
project(listat_task)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_CXX_FLAGS "-lpthread")

include_directories(include)

set(SOURCES
        src/main.cpp
        src/xml_parser.cpp
        src/xml_handler.cpp
        src/xml_exception.cpp
        src/primes_finder.cpp
        src/algorithms.cpp)

add_executable(${PROJECT_NAME} ${SOURCES})
