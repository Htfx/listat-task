/*
 * As it is only demo task I've decided not to perform
 * full test coverage
 */

#include "../../include/xml_handler.h"
#include "../../include/xml_exception.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using ::testing::AtLeast;

TEST(HandlerTest, SequenceHandle) {
    xml::Handler handler;

    handler.StartTag("high");
    handler.EndTag("high");
    ASSERT_THROW(handler.StartTag("low"), xml::Exception);
}

TEST(HandleTest, UnclosedHandle) {
    xml::Handler handler;

    handler.StartTag("high");
    ASSERT_THROW(handler.StartTag("low"), xml::Exception);
}