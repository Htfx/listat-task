/*
 * SAX API definition.
 * This API used only for purposes of this project.
 * Handler can be reimplemented to meet your specific requirements.
 * Remark: probably it would be better to create pure virtual Handler class
 * and use inherited project specific Handler class.
 * But this is only demo task, so I have decided to keep things simple.
 * Remark: values of intervals should be in range: 0..4294967295. If handling big numbes is required
 * class Handler must be reimplemented and use GNU multiple precision library or
 * any similar one.
 */

#ifndef LISTAT_TASK_SAX_HANDLER_H
#define LISTAT_TASK_SAX_HANDLER_H

#include "../include/primes_finder.h"
#include <string>
#include <utility>
#include <stack>

namespace xml {

class Handler {
public:
    explicit Handler();
    virtual ~Handler();

    // assuming that the default Handler can be changed to implement specific stuff
    virtual void StartDoc(std::string filename);
    virtual void EndDoc();
    virtual void StartTag(std::string tag);
    virtual void EndTag(std::string tag);
    virtual void Content(std::string content);

protected:
    std::stack <std::string> tags;

private:
    std::pair <unsigned long, unsigned long> interval;

    enum HandlerState {
        idle       = 0, // handling unnecessary tags
        save_low   = 1, // handling low tag
        low_saved  = 2, // low tag handled, waiting for high tag
        save_high  = 3, // handling high tag
        high_saved = 4  // high tag handled, we are ready to calculate prime numbers
    };
    HandlerState state;

    calc::PrimesFinder finder;
    std::string filename;
};

} // namespace xml

#endif //LISTAT_TASK_SAX_HANDLER_H
