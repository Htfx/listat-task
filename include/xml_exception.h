/*
 * Common XML exceptions
 */

#ifndef LISTAT_TASK_XML_EXCEPTION_H
#define LISTAT_TASK_XML_EXCEPTION_H

#include <string>
#include <exception>

namespace xml {

class Exception {
public:
    explicit Exception();
    explicit Exception(std::string error_msg);
    explicit Exception(std::string error_msg, std::string filename);

    std::string What() const noexcept;

private:
    std::string message;
    std::string filename;
};

} // namespace xml

#endif //LISTAT_TASK_XML_EXCEPTION_H
