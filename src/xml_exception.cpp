#include "../include/xml_exception.h"
#include <cstring>
#include <sstream>

xml::Exception::Exception() :
    message("Unknown XML excepion"), filename("") {}

xml::Exception::Exception(std::string error_msg) :
    message(error_msg), filename("") {}

xml::Exception::Exception(std::string error_msg, std::string filename) :
    message(error_msg), filename(filename) {}

std::string xml::Exception::What() const noexcept {
    std::ostringstream report;
    report <<  message << ". File: " << filename << std::endl;
    return report.str();
}