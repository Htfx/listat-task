#include <iostream>
#include "../include/xml_parser.h"
#include "../include/xml_handler.h"
#include "../include/xml_exception.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cout << "usage: ./listat_task [input file]";
    } else {
        xml::Handler my_handler;
        xml::Parser my_parser(&my_handler);
        my_parser.ParseFile(argv[1]);
    }
    return 0;
}