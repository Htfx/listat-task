#include "../include/xml_parser.h"
#include "../include/xml_exception.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <cctype>

xml::Parser::Parser(xml::Handler *chosen_handler) :
    handler(chosen_handler), state(idle) {}

void xml::Parser::ParseFile(std::string filename) {
    try {
        Parse(filename);
    } catch (xml::Exception &e) {
        std::cout << e.What() << std::endl;
    }
}

void xml::Parser::Parse(std::string filename) {
    std::fstream file;
    file.open(filename);

    char curr_char = '\0';
    std::string buffer;

    handler->StartDoc(filename);
    while (file >> curr_char) {
        if (curr_char == '<') {
            if (state == idle || state == pars_content) {
                handler->Content(buffer);
                buffer.clear();
                state = pars_op_tag;
                file >> curr_char;
                if (curr_char == '/') {
                    state = pars_cl_tag;
                } else {
                    file.putback(curr_char);
                }
            } else {
                throw (xml::Exception("Malformed XML tag", filename));

            }
        } else if (curr_char == '>') {
            if (state == pars_op_tag) {
                handler->StartTag(buffer);
            } else if (state == pars_cl_tag) {
                handler->EndTag(buffer);
            }
            buffer.clear();
            state = pars_content;
        } else {
            if (isalnum(curr_char)) {
                buffer += curr_char;
            }
        }
    }

    handler->EndDoc();

    file.close();
}

