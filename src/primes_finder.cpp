#include "../include/primes_finder.h"
#include "../include/algorithms.h"
#include <chrono>
#include <random>
#include <algorithm>
#include <iostream>
#include <cmath>

calc::PrimesFinder::PrimesFinder() :
    running_routines(false) {}

calc::PrimesFinder::~PrimesFinder() {
    // object can not be deleted until threads are running
    if (running_routines) {
        for (std::thread &rout: routines) {
            rout.join();
        }
    }
}

void calc::PrimesFinder::FindPrimes(unsigned long low, unsigned long high) {
    running_routines = true;
    routines.push_back(std::thread(PrimesSeekAlg, low, high, std::ref(prime_numbers), std::ref(lock)));
}

void calc::PrimesFinder::PrimesSeekAlg(unsigned long low, unsigned long high, std::vector <unsigned long> &storage,
                                       std::mutex &lock) {
    // not very accurate calculations. we need log2(n) iterations. where n is input numbers order
    unsigned long iterations = (unsigned long) std::log2((unsigned long) (low + (high - low) / 2));
    // even number can not be prime
    if (low % 2 == 0) {
        low++;
    }
    for (unsigned long i = low; i < high; i += 2) {
        if (PrimalityTest(i, iterations)) {
            std::lock_guard <std::mutex> section(lock);
            // adds only unique values
            if (std::find(storage.begin(), storage.end(), i) == storage.end()) {
                storage.push_back(i);
            }
        }
    }
}

std::vector <unsigned long> calc::PrimesFinder::GetAllPrimesFound() const {
    if (running_routines) {
        for (std::thread &rout: routines) {
            rout.join();
        }
    }
    running_routines = false;

    return prime_numbers;
}