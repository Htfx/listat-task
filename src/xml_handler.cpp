#include "../include/xml_handler.h"
#include <iostream>
#include <xml_exception.h>
#include <string>
#include <sstream>
#include <fstream>

xml::Handler::Handler() :
    state(idle) {}

xml::Handler::~Handler() {};

void xml::Handler::StartDoc(std::string file) {
    filename = file;
}

void xml::Handler::EndDoc() {
    std::vector<unsigned long> all_primes = finder.GetAllPrimesFound();

    std::ofstream file;
    file.open(filename, std::ios_base::app);
    file << std::endl << "<root>" << std::endl << "<primes> ";
    for (unsigned long prime: all_primes) {
        file << prime << " ";
    }
    file << " </primes>" << std::endl << "</root>" << std::endl;
    file.close();
}

void xml::Handler::StartTag(std::string next_tag) {
    if (tags.empty() && next_tag == "root") {
        tags.push(next_tag);
        state = idle;
    } else if (tags.top() == "root" && next_tag == "intervals") {
        tags.push(next_tag);
        state = idle;
    } else if (tags.top() == "intervals" && next_tag == "interval") {
        tags.push(next_tag);
        state = idle;
    } else if (tags.top() == "interval" && next_tag == "low") {
        tags.push(next_tag);
        state = save_low;
    } else if (tags.top() == "interval" && next_tag == "high" && state == low_saved) {
        tags.push(next_tag);
        state = save_high;
    } else {
        throw (xml::Exception("Wrong tags sequence", filename));
    }
}

void xml::Handler::EndTag(std::string tag) {
    if (tag == tags.top()) {
        tags.pop();
        if (state == high_saved) {
            finder.FindPrimes(interval.first, interval.second);
            state = idle;
        }
    } else {
        std::ostringstream report;
        report << "XML tag: <" << tags.top() << "> needs to be closed";
        throw (xml::Exception(report.str(), filename));
    }
}

void xml::Handler::Content(std::string content) {
    if (state == save_low || state == save_high) {
        unsigned long numerical_val = 0;
        try {
            numerical_val = std::stoul(content);
        } catch (std::out_of_range &err) {
            std::ostringstream report;
            report << "Too big interval value in tag: <" << tags.top() << "> Got '" << content << "'";
            throw (xml::Exception(report.str(), filename));
        } catch (std::invalid_argument &err) {
            std::ostringstream report;
            report << "Numerical argument required in tag: <" << tags.top() << "> Got '" << content << "' instead";
            throw (xml::Exception(report.str(), filename));
        }

        if (state == save_low) {
            interval.first = numerical_val;
            state = low_saved;
        } else if (state == save_high) {
            interval.second = numerical_val;
            state = high_saved;
        }
    }
}