#include "../include/algorithms.h"
#include <chrono>
#include <random>

unsigned long calc::PowMod(unsigned long base, unsigned long power, unsigned long modulo) {
    unsigned long power_iter = 1;
    while (power != 0) {
        if ((power % 2) == 0) {
            power = (unsigned long) power / 2;
            base = (base * base) % modulo;
        } else {
            power -= 1;
            power_iter = (power_iter * base) % modulo;
        }
    }
    return power_iter;
}

bool calc::PrimalityTest(unsigned long prime, unsigned iterations) {
    // initial conditions
    if (prime == 0) {
        return false;
    } else if (prime == 1) {
        return true;
    } else if (prime % 2 == 0) {
        return false;
    } else {
        // get prime - 1 as (2^s)*t (t is odd)
        unsigned long t = prime - 1;
        unsigned long s = 0;
        while ((t % 2) == 0) {
            t = (unsigned long) t / 2;
            s += 1;
        }
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution <unsigned long> distr(2, prime - 2);
        for (unsigned i = 0; i < iterations; i++) {
            unsigned long base = distr(gen);
            unsigned long exp = PowMod(base, t, prime);
            if (exp == 1 || exp == prime - 1) {
                continue;
            } else {
                for (unsigned j = 0; j < s - 1; j++) {
                    exp = PowMod(exp, 2, prime);
                    if (exp == 1) {
                        return false;
                    } else if (exp == prime - 1) {
                        break;
                    }
                }
                return false;
            }
        }
        return true;
    }
}